﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using LIB;

namespace UNITTEST
{
    [TestClass]
    public class ViewAndColor
    {
        [TestMethod]
        public void VC_1()
        {
            Application app=Application.GetApplication();
            app.Settings.Color = Application.Color.Black;
            app.Settings.View = Application.View.Adaptive;
            Assert.AreEqual(Application.Color.Black, app.Settings.Color, "Не удалось присвоить цвет");
            Assert.AreEqual(Application.View.Adaptive, app.Settings.View, "Не удалось присвоить представление");
        }
        [TestMethod]
        public void VC_2()
        {
            Application app = Application.GetApplication();
            app.Settings.Color = Application.Color.Black;
            app.Settings.View = Application.View.Compact;
            Assert.AreEqual(Application.Color.Black, app.Settings.Color, "Не удалось присвоить цвет");
            Assert.AreEqual(Application.View.Compact, app.Settings.View, "Не удалось присвоить представление");
        }
        [TestMethod]
        public void VC_3()
        {
            Application app = Application.GetApplication();
            app.Settings.Color = Application.Color.White;
            app.Settings.View = Application.View.Adaptive;
            Assert.AreEqual(Application.Color.White, app.Settings.Color, "Не удалось присвоить цвет");
            Assert.AreEqual(Application.View.Adaptive, app.Settings.View, "Не удалось присвоить представление");
        }
        [TestMethod]
        public void VC_4()
        {
            Application app = Application.GetApplication();
            app.Settings.Color = Application.Color.White;
            app.Settings.View = Application.View.Compact;
            Assert.AreEqual(Application.Color.White, app.Settings.Color, "Не удалось присвоить цвет");
            Assert.AreEqual(Application.View.Compact, app.Settings.View, "Не удалось присвоить представление");
        }
    }
}

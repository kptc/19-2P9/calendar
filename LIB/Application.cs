﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIB
{
    public class Application
    {
        private static Application application;
        public static Application GetApplication()
        {
            if (application == null)
                application = new Application();
            return application;
        }
        public enum Color {White, Black}
        public enum View {Adaptive, Compact}
        public Settings Settings = new Settings();
    }
}

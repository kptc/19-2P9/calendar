﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPF
{
    /// <summary>
    /// Логика взаимодействия для ViewAndColor.xaml
    /// </summary>
    public partial class ViewAndColor : Window
    {
        LIB.Application app;

        public ViewAndColor()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void view_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            app.Settings.View = (LIB.Application.View)view.SelectedIndex;
        }

        private void color_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            app.Settings.Color = (LIB.Application.Color)color.SelectedIndex;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            app = LIB.Application.GetApplication();
            view.SelectedIndex = (int)app.Settings.View;
            color.SelectedIndex = (int)app.Settings.Color;
        }
    }
}
